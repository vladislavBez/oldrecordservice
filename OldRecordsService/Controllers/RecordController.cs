﻿using OldRecordsService.BLL.ModelServises;
using OldRecordsService.Models.RecordModels;
using OldRecordsService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OldRecordsService.Controllers
{
    public class RecordController : Controller
    {
        // GET: Record/RecordList
        [Authorize]
        public ActionResult RecordList(int? page, int? pageSize, string showTableCols, string sortOrder, string sortDirection, string positions, string statuses, string vacancies, bool? showOldRecord, string dateFrom, string dateTo, string search)
        {

            RecordService recordService = new RecordService();
            RecordListViewModel records = recordService.GetResumeList(page, pageSize, showTableCols, sortOrder, sortDirection, positions, statuses, vacancies, showOldRecord, dateFrom, dateTo, search);
            
            return View("RecordList", records);
        }

        // GET: Record/SingleRecord
        [Authorize]
        public ActionResult SingleRecord(int? id, int? page, int? pageSize, string showTableCols, string sortOrder, string sortDirection, string positions, string statuses, string vacancies, bool? showOldRecord, string dateFrom, string dateTo, string search)
        {
            ViewResult viewResult = new ViewResult();
            RecordService recordService = new RecordService();

            if (id != null)
            {
                RecordListViewModel record = recordService.GetResumeById(id, page, pageSize, showTableCols, sortOrder, sortDirection, positions, statuses, vacancies, showOldRecord, dateFrom, dateTo, search);
                viewResult = View("SingleRecord", record);
            }
            else
            {
                viewResult = View("NotFoundRecord");
            }           

            return viewResult;
        }

        // GET: Record/UpdateRecords
        public string UpdateRecords()
        {
            RecordService recordService = new RecordService();
            recordService.UploadCsvFiles();
            return "";
        }
    }
}