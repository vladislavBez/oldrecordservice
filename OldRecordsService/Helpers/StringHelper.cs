﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Helpers
{
    public static class StringHelper
    {
        public static string RemoveUnnecessaryChars(string text)
        {
            string res = "";
            string[] charsToRemove = new string[] { "\"" };

            foreach (var singleChar in charsToRemove)
            {
                res = text.Replace(singleChar, string.Empty);
                if (res == "empty")
                {
                    res = String.Empty;
                }
            }

            return res;
        }

        public static List<string> StringArrayToList(string stringArray)
        {
            List<string> res = new List<string>();

            if (!String.IsNullOrEmpty(stringArray))
            {
                Char delimiter = ',';

                string[] arr = stringArray.Split(delimiter);

                foreach (var item in arr)
                {
                    res.Add(item);
                }
            }            

            return res;
        }

        public static List<bool> StringArrayToBoolList(string stringArray)
        {
            List<bool> res = new List<bool>();

            if (!String.IsNullOrEmpty(stringArray))
            {
                Char delimiter = ',';

                string[] arr = stringArray.Split(delimiter);

                foreach (var item in arr)
                {
                    if (item == "true")
                    {
                        res.Add(true);
                    }
                    else
                    {
                        res.Add(false);
                    }                    
                }
            }

            return res;
        }
    }
}