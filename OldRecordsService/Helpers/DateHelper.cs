﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Helpers
{
    public static class DateHelper
    {
        public static DateTime? GetDateFromString(string dateStr)
        {
            DateTime? date = null;

            if ((dateStr != "0000-00-00") && (dateStr != "empty"))
            {
                try
                {
                    int year = Int32.Parse(dateStr.Substring(0, 4));
                    int mount = Int32.Parse(dateStr.Substring(5, 2));
                    int day = Int32.Parse(dateStr.Substring(8, 2));
                    date = new DateTime(year, mount, day);
                }
                catch (Exception ex)
                {
                    date = null;
                    Console.WriteLine("IOException source: {0}", ex.Message);
                }
            }
            
            return date;
        }

        public static DateTime GetYearFromString(string yearStr)
        {
            DateTime date = new DateTime();
            if (yearStr != "0000")
            {
                int year = Int32.Parse(yearStr);
                date = new DateTime(year, 1, 1);
            }
            return date;
        }

        public static string GetTodayStringFormat()
        {
            string todayStr = DateTime.Today.ToString("dd-MM-yy");
            return todayStr;
        }
    }    
}