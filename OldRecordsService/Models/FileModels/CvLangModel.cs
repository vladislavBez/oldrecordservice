﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.FileModels
{
    [DelimitedRecord("#")]
    [IgnoreEmptyLines]
    public class CvLangModel
    {
        public int IdOld { get; set; }
        public int CvId { get; set; }
        public string Name { get; set; }
        public string Extent { get; set; }
    }
}