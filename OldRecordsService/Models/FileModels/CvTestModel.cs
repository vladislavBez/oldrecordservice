﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.FileModels
{
    [DelimitedRecord("`")]
    [IgnoreEmptyLines]
    public class CvTestModel
    {
        public int IdOld { get; set; }
        public int CvId { get; set; }
        public string Name { get; set; }
        public string Organisation { get; set; }
        public string Result { get; set; }
        //Date format "yyyy-mm-dd"
        public string EndYear { get; set; }
    }
}