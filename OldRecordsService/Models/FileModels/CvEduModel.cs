﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.FileModels
{
    [DelimitedRecord("`")]
    [IgnoreEmptyLines]
    public class CvEduModel
    {
        public int IdOld { get; set; }
        public int CvId { get; set; }
        public string Name { get; set; }
        public string FacName { get; set; }
        public string Diploma { get; set; }
        public string EndYear { get; set; }
    }
}