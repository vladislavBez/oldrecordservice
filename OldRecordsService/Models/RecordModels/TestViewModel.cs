﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class TestViewModel
    {
        public int Id { get; set; }
        public int IdOld { get; set; }
        public int? CvId { get; set; }
        public string Name { get; set; }
        public string Organisation { get; set; }
        public string Result { get; set; }
        public string EndYear { get; set; }
    }
}