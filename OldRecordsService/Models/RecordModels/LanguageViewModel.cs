﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class LanguageViewModel
    {
        public int Id { get; set; }
        public int IdOld { get; set; }
        public int? CvId { get; set; }
        public string Name { get; set; }
        public string Extent { get; set; }
    }
}