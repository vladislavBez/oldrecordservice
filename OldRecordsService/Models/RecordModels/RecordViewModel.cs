﻿using OldRecordsService.Models.RecordModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class RecordViewModel
    {
        public int Id { get; set; }
        public int IdOld { get; set; }
        public StatusViewModel Status { get; set; }
        public string FirstName { get; set; }
        public string Title { get; set; }
        public string SurName { get; set; }
        public int Age { get; set; }
        public DateTime? Birthday { get; set; }
        public List<EducationViewModel> Education { get; set; }
        public string Vacancy { get; set; }
        public List<ExperienceViewModel> Experience { get; set; }
        public string Salary { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
        public bool? IsSend { get; set; }
        public DateTime? SendDate { get; set; }
        public string Hobby { get; set; }
        public string Sport { get; set; }
        public string SportProgress { get; set; }
        public string KomatsuComment { get; set; }
        public string Army { get; set; }
        public string Driver { get; set; }
        public string OccupationalSafetyHealth { get; set; }
        public bool IsViewed { get; set; }
        public bool IsAgreePersonalData { get; set; }
        public ExperienceAvailableViewModel ExperienceAvailable { get; set; }
        public EducationAvailableViewModel EducationAvailable { get; set; }
        public List<LanguageViewModel> Languages { get; set; }
        public List<TestViewModel> Tests { get; set; }
    }
}