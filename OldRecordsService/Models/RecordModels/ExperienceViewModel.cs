﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class ExperienceViewModel
    {
        public int Id { get; set; }
        public int IdOld { get; set; }
        public int? CvId { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Organisation { get; set; }
        public string Position { get; set; }
        public string Duty { get; set; }
    }
}