﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class FilterViewModel
    {
        private List<bool> showTableCols;

        public string Statuses { get; set; }
        public string Vacancies { get; set; }
        public string Search { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public bool ShowOldRecords { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SortOrder { get; set; }
        public string SortDirection { get; set; }
        public List<bool> ShowTableCols
        {
            get { return showTableCols; }
            set
            {
                if ((value == null) || (value.Count == 0))
                {
                    showTableCols = new List<bool>();
                    for (int i = 0; i <= 13; i++)
                    {
                        showTableCols.Add(true);
                    }
                }
                else
                {
                    showTableCols = value;
                }
            }
        }
    }
}