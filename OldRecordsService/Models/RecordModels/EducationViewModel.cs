﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class EducationViewModel
    {
        public int Id { get; set; }
        public int IdOld { get; set; }
        public int? CvId { get; set; }
        public string Name { get; set; }
        public string FacultyName { get; set; }
        public string Diploma { get; set; }
        public string EndYear { get; set; }
    }
}