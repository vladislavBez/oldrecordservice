﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class StatusViewModel
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string Color { get; set; }
    }
}