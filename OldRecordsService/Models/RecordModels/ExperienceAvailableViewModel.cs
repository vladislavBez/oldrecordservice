﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class ExperienceAvailableViewModel
    {
        public int Id { get; set; }
        public string Period { get; set; }
    }
}