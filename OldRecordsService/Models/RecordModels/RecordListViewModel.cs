﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class RecordListViewModel
    {
        public IPagedList<RecordViewModel> Records { get; set; }
        public RecordViewModel SingleRecord { get; set; }
        public List<StatusViewModel> Statuses { get; set; }
        public List<RecordViewModel> Vacancies { get; set; }
        public List<ExperienceViewModel> Positions { get; set; }
        public int CountRecords { get; set; }
        public FilterViewModel Filters { get; set; }
    }
}