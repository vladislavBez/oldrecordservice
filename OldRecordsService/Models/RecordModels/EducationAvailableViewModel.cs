﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.RecordModels
{
    public class EducationAvailableViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}