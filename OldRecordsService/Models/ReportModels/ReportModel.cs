﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Models.ReportModels
{
    public class ReportModel
    {
        public bool IsCreateDirectory { get; set; }
        public string CreateDirectoryExceptionMessage { get; set; }

        public bool IsConnectFTPSuccess { get; set; }
        public string ConnectFTPExceptionMessage { get; set; }

        public bool IsDownloadCvSuccess { get; set; }
        public bool IsDownloadCvEduSuccess { get; set; }
        public bool IsDownloadCvLangSuccess { get; set; }
        public bool IsDownloadCvProfSuccess { get; set; }
        public bool IsDownloadCvTestSuccess { get; set; }
        public string DownloadCvExceptionMessage { get; set; }

        public bool IsSaveToDBSuccess { get; set; }
        public string SaveToDBExceptionMessage { get; set; }

    }
}