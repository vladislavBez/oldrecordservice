﻿using FileHelpers;
using FluentFTP;
using OldRecordService.Models.FileModels;
using OldRecordsService.BLL.ModelServises;
using OldRecordsService.Constants;
using OldRecordsService.Helpers;
using OldRecordsService.Models.FileModels;
using OldRecordsService.Models.RecordModels;
using OldRecordsService.Models.ReportModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Hosting;

namespace OldRecordsService.Services
{
    public class RecordService
    {
        public RecordListViewModel GetResumeList(int? page, int? pageSize, string showTableCols, string sortOrder, string sortDirection, string positions, string statuses, string vacancies, bool? showOldRecord, string dateFrom, string dateTo, string search)
        {
            RecordListViewModel recordList = new RecordListViewModel();

            int pageNumber = GlobalConstants.DEFAULT_PAGE_NUMBER;
            int pageCountRow = GlobalConstants.DEFAULT_ROW_COUNT;

            if (page != null)
            {
                pageNumber = (int)page;
            }

            if (pageSize != null)
            {
                pageCountRow = (int)pageSize;
            }

            if (positions == null)
            {
                positions = String.Empty;
            }

            if (statuses == null)
            {
                statuses = String.Empty;
            }

            if(vacancies == null)
            {
                vacancies = String.Empty;
            }

            if (showOldRecord == null)
            {
                showOldRecord = false;
            }

            if (dateFrom == null)
            {
                dateFrom = String.Empty;
            }

            if (dateTo == null)
            {
                dateTo = String.Empty;
            }

            if (search == null)
            {
                search = String.Empty;
            }

            List<RecordViewModel> records = new List<RecordViewModel>();
            RecordModelService recordModelService = new RecordModelService();
            records = recordModelService.GetResumeList(sortOrder, sortDirection, StringHelper.StringArrayToList(positions), StringHelper.StringArrayToList(statuses), StringHelper.StringArrayToList(vacancies), showOldRecord, DateHelper.GetDateFromString(dateFrom), DateHelper.GetDateFromString(dateTo), search, false, false);

            recordList.Records = records.ToPagedList(pageNumber, pageCountRow);
            recordList.Statuses = recordModelService.GetStatusList();
            recordList.Vacancies = recordModelService.GetVacancyList();
            recordList.CountRecords = records.Count;

            recordList.Filters = new FilterViewModel
            {
                DateFrom = dateFrom,
                DateTo = dateTo,
                Search = search,
                ShowOldRecords = (bool)showOldRecord,
                Statuses = statuses,
                Vacancies = vacancies,
                Page = pageNumber,
                PageSize = pageCountRow,
                SortOrder = sortOrder,
                SortDirection = sortDirection,
                ShowTableCols = StringHelper.StringArrayToBoolList(showTableCols)
            };

            return recordList;
        }

        public RecordListViewModel GetResumeById(int? id, int? page, int? pageSize, string showTableCols, string sortOrder, string sortDirection, string positions, string statuses, string vacancies, bool? showOldRecord, string dateFrom, string dateTo, string search)
        {
            RecordListViewModel recordList = new RecordListViewModel();
            RecordModelService recordModelService = new RecordModelService();

            int pageNumber = GlobalConstants.DEFAULT_PAGE_NUMBER;
            int pageCountRow = GlobalConstants.DEFAULT_ROW_COUNT;

            if (page != null)
            {
                pageNumber = (int)page;
            }

            if (pageSize != null)
            {
                pageCountRow = (int)pageSize;
            }

            if (positions == null)
            {
                positions = String.Empty;
            }

            if (statuses == null)
            {
                statuses = String.Empty;
            }

            if (vacancies == null)
            {
                vacancies = String.Empty;
            }

            if (showOldRecord == null)
            {
                showOldRecord = false;
            }

            if (dateFrom == null)
            {
                dateFrom = String.Empty;
            }

            if (dateTo == null)
            {
                dateTo = String.Empty;
            }

            if (search == null)
            {
                search = String.Empty;
            }

            int idRecord;
            if (id != null)
            {
                idRecord = (int)id;
                recordList.SingleRecord = recordModelService.GetResumeById(idRecord);
            }
            
            recordList.Statuses = recordModelService.GetStatusList();
            recordList.Vacancies = recordModelService.GetVacancyList();

            recordList.Filters = new FilterViewModel
            {
                DateFrom = dateFrom,
                DateTo = dateTo,
                Search = search,
                ShowOldRecords = (bool)showOldRecord,
                Statuses = statuses,
                Vacancies = vacancies,
                Page = pageNumber,
                PageSize = pageCountRow,
                SortOrder = sortOrder,
                SortDirection = sortDirection,
                ShowTableCols = StringHelper.StringArrayToBoolList(showTableCols)
            };

            return recordList;
        }

        public void UploadCsvFiles()
        {
            FtpClient client = new FtpClient();

            //Create report
            ReportModel report = new ReportModel();

            //Create folder name (dotay in format dd-mm-yy 28-08-18)
            string todayStr = DateHelper.GetTodayStringFormat();

            try
            {
                //Create folder
                Directory.CreateDirectory(HostingEnvironment.MapPath("~") + "/App_Data/Uploads/" + todayStr);
                report.IsCreateDirectory = true;
            }
            catch (Exception ex)
            {
                report.IsCreateDirectory = false;
                report.CreateDirectoryExceptionMessage = ex.Message;
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            
            //Create file names
            string uploadFolderName = HostingEnvironment.MapPath("~") + GlobalConstants.PATH_TO_UPLOADS_FOLDER + todayStr+"\\";
            string remotePathCvFile = GlobalConstants.PATH_TO_DOWNLOADS_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV + ".csv";
            string remotePathCvEduFile = GlobalConstants.PATH_TO_DOWNLOADS_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_EDU + ".csv";
            string remotePathCvLangFile = GlobalConstants.PATH_TO_DOWNLOADS_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_LANG + ".csv";
            string remotePathCvProfFile = GlobalConstants.PATH_TO_DOWNLOADS_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_PROF + ".csv";
            string remotePathCvTestFile = GlobalConstants.PATH_TO_DOWNLOADS_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_TEST + ".csv";

            try
            {
                //Connect FTP
                client.Host = GlobalConstants.FTP_SERVER;
                client.Credentials = new NetworkCredential(GlobalConstants.FTP_LOGIN, GlobalConstants.FTP_PASSWORD);
                client.Port = GlobalConstants.FTP_PORT;

                client.Connect();

                report.IsConnectFTPSuccess = true;
            }
            catch (Exception ex)
            {
                report.IsConnectFTPSuccess = false;
                report.ConnectFTPExceptionMessage = ex.Message;
                Console.WriteLine("IOException source: {0}", ex.Message);
            }

            try
            {
                //Download files from server
                report.IsDownloadCvSuccess = client.DownloadFile(uploadFolderName+ remotePathCvFile.Split('/').Last(), remotePathCvFile, false, FtpVerify.Retry);
                report.IsDownloadCvSuccess = client.DownloadFile(uploadFolderName + remotePathCvEduFile.Split('/').Last(), remotePathCvEduFile, false, FtpVerify.Retry);
                report.IsDownloadCvSuccess = client.DownloadFile(uploadFolderName + remotePathCvLangFile.Split('/').Last(), remotePathCvLangFile, false, FtpVerify.Retry);
                report.IsDownloadCvSuccess = client.DownloadFile(uploadFolderName + remotePathCvProfFile.Split('/').Last(), remotePathCvProfFile, false, FtpVerify.Retry);
                report.IsDownloadCvSuccess = client.DownloadFile(uploadFolderName + remotePathCvTestFile.Split('/').Last(), remotePathCvTestFile, false, FtpVerify.Retry);
            }
            catch (Exception ex)
            {
                report.DownloadCvExceptionMessage = ex.Message;
                Console.WriteLine("IOException source: {0}", ex.Message);
            }

            //Disonnect FTP
            client.Disconnect();

            try
            {
                //Save file to DB
                SaveCvToDB();
                SaveCvEduToDB();
                SaveCvLangToDB();
                SaveCvTestToDB();
                SaveCvProfToDB();

                report.IsSaveToDBSuccess = true;
            }
            catch (Exception ex)
            {
                report.IsSaveToDBSuccess = false;
                report.SaveToDBExceptionMessage = ex.Message;
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
        }
        
        private void SaveCvToDB()
        {
            //Get new records
            string todayStr = DateHelper.GetTodayStringFormat();
            string pathCvFile = HostingEnvironment.MapPath("~") + GlobalConstants.PATH_TO_UPLOADS_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV + ".csv";
            FileHelperEngine<CvModel> engine = new FileHelperEngine<CvModel>(Encoding.UTF8);
            CvModel[] recordsNew = engine.ReadFile(pathCvFile);

            //Save all new entries
            RecordModelService recordModelService = new RecordModelService();
            foreach (var record in recordsNew)
            {
                if (!recordModelService.GetIsResumeExist(record.IdOld))
                {
                    recordModelService.SetResume(record);
                }
            }
        }

        private void SaveCvEduToDB()
        {
            //Get new records
            string todayStr = DateHelper.GetTodayStringFormat();
            string pathCvEduFile = HostingEnvironment.MapPath("~") + GlobalConstants.PATH_TO_UPLOADS_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_EDU + ".csv";
            FileHelperEngine<CvEduModel> engine = new FileHelperEngine<CvEduModel>(Encoding.UTF8);
            CvEduModel[] recordsNew = engine.ReadFile(pathCvEduFile);

            //Save all new entries
            RecordModelService recordModelService = new RecordModelService();
            foreach (var record in recordsNew)
            {
                if (!recordModelService.GetIsEducationExist(record.IdOld))
                {
                    recordModelService.SetEducation(record);
                }
            }
        }

        private void SaveCvLangToDB()
        {
            //Get new records
            string todayStr = DateHelper.GetTodayStringFormat();
            string pathCvLangFile = HostingEnvironment.MapPath("~") + GlobalConstants.PATH_TO_UPLOADS_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_LANG + ".csv";
            FileHelperEngine<CvLangModel> engine = new FileHelperEngine<CvLangModel>(Encoding.UTF8);
            CvLangModel[] recordsNew = engine.ReadFile(pathCvLangFile);

            //Save all new entries
            RecordModelService recordModelService = new RecordModelService();
            foreach (var record in recordsNew)
            {
                if (!recordModelService.GetIsLanguageExist(record.IdOld))
                {
                    recordModelService.SetLanguage(record);
                }
            }
        }

        private void SaveCvTestToDB()
        {
            //Get new records
            string todayStr = DateHelper.GetTodayStringFormat();
            string pathCvTestFile = HostingEnvironment.MapPath("~") + GlobalConstants.PATH_TO_UPLOADS_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_TEST + ".csv";
            FileHelperEngine<CvTestModel> engine = new FileHelperEngine<CvTestModel>(Encoding.UTF8);
            CvTestModel[] recordsNew = engine.ReadFile(pathCvTestFile);

            //Save all new entries
            RecordModelService recordModelService = new RecordModelService();
            foreach (var record in recordsNew)
            {
                if (!recordModelService.GetIsTestExist(record.IdOld))
                {
                    recordModelService.SetTest(record);
                }
            }
        }

        private void SaveCvProfToDB()
        {
            //Get new records
            string todayStr = DateHelper.GetTodayStringFormat();
            string pathCvProfFile = HostingEnvironment.MapPath("~") + GlobalConstants.PATH_TO_UPLOADS_FOLDER + todayStr + "/" + todayStr + GlobalConstants.FILE_NAME_CV_PROF + ".csv";
            FileHelperEngine<CvProfModel> engine = new FileHelperEngine<CvProfModel>(Encoding.UTF8);
            CvProfModel[] recordsNew = engine.ReadFile(pathCvProfFile);

            //Save all new entries
            RecordModelService recordModelService = new RecordModelService();
            foreach (var record in recordsNew)
            {
                if (!recordModelService.GetIsExperienceExist(record.IdOld))
                {
                    recordModelService.SetExperience(record);
                }
            }
        }       

    }
}