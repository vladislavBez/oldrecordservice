$(document).ready(function () {

    //initPopoverTable
    function initPopoverTable () {
        
        var $popover = $("#container-popover-0");
        var $popoverButton = $("#popover-button-0");

        $popoverButton.click(function () {
            
            if ($popover.hasClass("hide")) {
                $popover.removeClass("hide");
            } else {
                $popover.addClass("hide");
            }

        });

         //Close widget when clicking outside the area
        $(document).mouseup(function (e){
            if (!$popover.is(e.target)
                && $popover.has(e.target).length === 0
                && !$popover.hasClass("hide")
                && !$popoverButton.is(e.target) 
                && $popoverButton.has(e.target).length === 0) { 
                $popover.addClass("hide");;
            }
        });
    }    

    //initPopoverPosition
    function initPopoverPosition () {
        
        var $popover = $("#container-popover-1");
        var $popoverButton = $("#search-field-position");

        $popoverButton.focus(function () {
            
            if ($popover.hasClass("hide")) {
                $popover.removeClass("hide");
            } else {
                $popover.addClass("hide");
            }

        });

         //Close widget when clicking outside the area
        $(document).mouseup(function (e){
            if (!$popover.is(e.target)
                && $popover.has(e.target).length === 0
                && !$popover.hasClass("hide")
                && !$popoverButton.is(e.target) 
                && $popoverButton.has(e.target).length === 0) { 
                $popover.addClass("hide");;
            }
        });
    }    

    //initPopoverStatus
    function initPopoverStatus () {
        
        var $popover = $("#container-popover-2");
        var $popoverButton = $("#search-field-status");

        $popoverButton.focus(function () {
            
            if ($popover.hasClass("hide")) {
                $popover.removeClass("hide");
            } else {
                $popover.addClass("hide");
            }

        });

         //Close widget when clicking outside the area
        $(document).mouseup(function (e){
            if (!$popover.is(e.target)
                && $popover.has(e.target).length === 0
                && !$popover.hasClass("hide")
                && !$popoverButton.is(e.target) 
                && $popoverButton.has(e.target).length === 0) { 
                $popover.addClass("hide");;
            }
        });
    }    

    //initPopoverVacancy
    function initPopoverVacancy () {
        
        var $popover = $("#container-popover-3");
        var $popoverButton = $("#search-field-vacancy");

        $popoverButton.focus(function () {
            
            if ($popover.hasClass("hide")) {
                $popover.removeClass("hide");
            } else {
                $popover.addClass("hide");
            }

        });

         //Close widget when clicking outside the area
        $(document).mouseup(function (e){
            if (!$popover.is(e.target)
                && $popover.has(e.target).length === 0
                && !$popover.hasClass("hide")
                && !$popoverButton.is(e.target) 
                && $popoverButton.has(e.target).length === 0) { 
                $popover.addClass("hide");;
            }
        });
    }    
    
    //initPopoverTable
    initPopoverTable();
    //initPopoverPosition
    initPopoverPosition();
    //initPopoverStatus
    initPopoverStatus();
    //initPopoverVacancy
    initPopoverVacancy();

});