$(document).ready(function () {

    //Init Select Table Col
    function initSelectTableCol () {
        
        var allChecbox = $(".checkbox-target-col");

        allChecbox.each(function(index, element){

            var $checbox = $(element);
            var targetCol = $checbox.attr("target-col");

            $checbox.click(function () {
                if ($checbox.is(':checked')) {
                    $("[col-number = " + targetCol + "]").removeClass("hide");
                } else {
                    $("[col-number = " + targetCol + "]").addClass("hide");
                }   
            });

        });
    }    
    
    //Select Table Col
    initSelectTableCol();

});