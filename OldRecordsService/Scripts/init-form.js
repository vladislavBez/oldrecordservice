﻿$(document).ready(function () {

    var options = {
        urlRecordList: "http://185.242.179.194:80/Record/RecordList",
        urlSingleRecord: "http://185.242.179.194:80/Record/SingleRecord",
    }

    function initHeaderActions() {

        $("#header-action-print").click(function () {
            window.print();
        });

    }

    function initSelectInputs() {
        $("#search-field-position").select2({
            width: '100%'
        });
        $("#search-field-vacancy").select2({
            width: '100%'
        });
        $("#search-field-status").select2({
            width: '100%'
        });
        $("#select-count-row").select2({
            width: '90px'
        });
        $("#select-count-row").change(function () {
            getRecordList();
        });
    }

    function getValueArray(select2Array) {

        var valueArray = [];

        select2Array.forEach(function (item, i, arr) {
            valueArray[i] = item.text;
        });

        return valueArray;
    }

    function getStringValue(select2Array) {

        var stringValue = "";

        select2Array.forEach(function (item, i, arr) {
            stringValue = stringValue + item.text + ",";
        });

        stringValue = stringValue.substring(0, stringValue.length - 1);

        return stringValue;
    }

    function getStringValuePageSize(select2Array) {

        var stringValue = "";

        select2Array.forEach(function (item, i, arr) {
            var itemValue = item.text;
            if (itemValue == "Все") {
                itemValue = "999999"
            }
            stringValue = stringValue + itemValue + ",";
        });

        stringValue = stringValue.substring(0, stringValue.length - 1);

        return stringValue;
    }

    function getShowTableCols() {

        var stringValue = "";
        var allCheckbox = $(".checkbox-target-col");

        allCheckbox.each(function (index, element) {
            var $checkbox = $(element);
            stringValue = stringValue + ($checkbox.is(':checked') ? "true" : "false") + ",";
        });

        stringValue = stringValue.substring(0, stringValue.length - 1);

        return stringValue;
    }

    function getRecordList() {

        var selectVacancies = getStringValue($("#search-field-vacancy").select2('data'));
        var selectStatuses = getStringValue($("#search-field-status").select2('data'));
        var searchString = $("#search-field-common").val();
        var dateFrom = $("#search-field-date-from").val();
        var dateTo = $("#search-field-date-to").val();
        var showOldRecord = $("#search-field-old").is(':checked') ? true : false;
        var pageSize = getStringValuePageSize($("#select-count-row").select2('data'));
        var sortOrder = $("#records-sort-order").text();
        var sortDirection = $("#records-sort-direction").text();
        var showTableCols = getShowTableCols();

        var url = options.urlRecordList + "?" + $.param({
            page: null,
            pageSize: pageSize,
            showTableCols: showTableCols,
            sortOrder: sortOrder,
            sortDirection: sortDirection,
            positions: null,
            statuses: selectStatuses,
            vacancies: selectVacancies,
            showOldRecord: showOldRecord,
            dateFrom: dateFrom,
            dateTo: dateTo,
            search: searchString
        });

        location.href = url;
    }

    function getSingleRecord(id) {

        var selectVacancies = getStringValue($("#search-field-vacancy").select2('data'));
        var selectStatuses = getStringValue($("#search-field-status").select2('data'));
        var searchString = $("#search-field-common").val();
        var dateFrom = $("#search-field-date-from").val();
        var dateTo = $("#search-field-date-to").val();
        var showOldRecord = $("#search-field-old").is(':checked') ? true : false;
        var sortOrder = $("#records-sort-order").text();
        var sortDirection = $("#records-sort-direction").text();
        var showTableCols = getShowTableCols();

        var page = Number($("#page-value").text());
        var pageSize = Number($("#page-size-value").text());

        var url = options.urlSingleRecord + "?" + $.param({
            id: id,
            page: page,
            pageSize: pageSize,
            showTableCols: showTableCols,
            sortOrder: sortOrder,
            sortDirection: sortDirection,
            positions: null,
            statuses: selectStatuses,
            vacancies: selectVacancies,
            showOldRecord: showOldRecord,
            dateFrom: dateFrom,
            dateTo: dateTo,
            search: searchString
        });

        location.href = url;
    }

    function initSubmitForm() {

        var $buttonSubmit = $("#button-submit");

        $buttonSubmit.click(function () {
            getRecordList();            
        });
    }

    function initSingleRecordLink() {

        var allSingleRecordLink = $(".single-record-link");

        allSingleRecordLink.click(function () {

            var id = $(this).attr("id-record");

            getSingleRecord(id);
        });
    }

    function initPanaginationLink() {

        var selectVacancies = getStringValue($("#search-field-vacancy").select2('data'));
        var selectStatuses = getStringValue($("#search-field-status").select2('data'));
        var searchString = $("#search-field-common").val();
        var dateFrom = $("#search-field-date-from").val();
        var dateTo = $("#search-field-date-to").val();
        var showOldRecord = $("#search-field-old").is(':checked') ? true : false;

        var $panaginationList = $(".panagination-list");
        var recordsCount = Number($("#records-count-value").text());
        var page = Number($("#page-value").text());
        var pageSize = Number($("#page-size-value").text());
        var sortOrder = $("#records-sort-order").text();
        var sortDirection = $("#records-sort-direction").text();
        var showTableCols = getShowTableCols();

        var countLink = recordsCount / pageSize;

        for (pageNumber = 1; pageNumber < countLink; pageNumber++) {

            var url = options.urlRecordList + "?" + $.param({
                page: pageNumber,
                pageSize: pageSize,
                showTableCols: showTableCols,
                sortOrder: sortOrder,
                sortDirection: sortDirection,
                positions: null,
                statuses: selectStatuses,
                vacancies: selectVacancies,
                showOldRecord: showOldRecord,
                dateFrom: dateFrom,
                dateTo: dateTo,
                search: searchString
            });

            var classActive = "";
            if (pageNumber == page) {
                classActive = "active";
            }

            $panaginationList.append(
                '<li class="' + classActive + '"><a href="' + url + '" target-page="' + pageNumber + '" class="panagination-link">' + pageNumber + '</a></li>'
            );
        }
    }   

    function setSortTable(tableHeader) {

        var $tableHeader = $(tableHeader);
        var colNumber = $tableHeader.attr("col-number");
        var sortDirection;

        if ($tableHeader.hasClass("content-table-sort-asc")) {
            sortDirection = "desc";
        } else {
            sortDirection = "asc";
        }

        $("#records-sort-order").text(colNumber);
        $("#records-sort-direction").text(sortDirection);

        getRecordList();

    }

    function initTableSort() {

        var allTableHeaders = $(".content-table-head");
        var sortOrder = $("#records-sort-order").text();
        var sortDirection = $("#records-sort-direction").text(); 

        var $currentTableHeader;
        if (sortOrder != "") {
            $currentTableHeader = $(".content-table-head[col-number=" + sortOrder + "]");
            if (sortDirection == "desc") {
                $currentTableHeader.addClass("content-table-sort-desc");
            }
            if (sortDirection == "asc") {
                $currentTableHeader.addClass("content-table-sort-asc");
            }
        }

        allTableHeaders.each(function (index, element) {
            var $colHeader = $(element);
            $colHeader.click(function () {
                setSortTable(this);
            });
        });

    }

    initSelectInputs();
    initSubmitForm();
    initSingleRecordLink();
    initHeaderActions();
    initPanaginationLink();
    initTableSort();
});