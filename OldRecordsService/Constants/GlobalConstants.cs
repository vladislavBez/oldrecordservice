﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.Constants
{
    public static class GlobalConstants
    {
        public const string PATH_TO_UPLOADS_FOLDER = "\\App_Data\\Uploads\\";
        public const string PATH_TO_DOWNLOADS_FOLDER = "/www/Record/";
        public const string FILE_NAME_CV = "-cv";
        public const string FILE_NAME_CV_EDU = "-cv-edu";
        public const string FILE_NAME_CV_LANG = "-cv-lang";
        public const string FILE_NAME_CV_PROF = "-cv-prof";
        public const string FILE_NAME_CV_TEST = "-cv-test";

        public const string FTP_SERVER = "81.177.140.122";
        public const int FTP_PORT = 21;
        public const string FTP_LOGIN = "gka-new_alp29";
        public const string FTP_PASSWORD = "5ed8363cda1990";

        public const int DEFAULT_PAGE_NUMBER = 1;
        public const int DEFAULT_ROW_COUNT = 20;
    }
}