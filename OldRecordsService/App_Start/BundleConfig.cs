﻿using System.Web;
using System.Web.Optimization;

namespace OldRecordsService
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. на странице https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // готово к выпуску, используйте средство сборки по адресу https://modernizr.com, чтобы выбрать только необходимые тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-js").Include(
                      "~/Scripts/jquery-3.3.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-js").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap-css").Include(
                      "~/Content/bootstrap-theme.min.css",
                      "~/Content/bootstrap.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/widgets-js").Include(
                      "~/Scripts/popover.js",
                      "~/Scripts/responsive-table.js",
                      "~/Scripts/init-form.js",
                      "~/Scripts/select-table-col.js"));

            bundles.Add(new StyleBundle("~/Content/main-css").Include(
                      "~/Content/header.css",
                      "~/Content/main.css",
                      "~/Content/popover.css",
                      "~/Content/responsive-table.css"));
        }
    }
}
