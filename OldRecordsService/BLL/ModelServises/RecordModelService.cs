﻿using OldRecordService.Models.FileModels;
using OldRecordsService.BLL.Interfaces;
using OldRecordsService.BLL.Repositories;
using OldRecordsService.Helpers;
using OldRecordsService.Models;
using OldRecordsService.Models.FileModels;
using OldRecordsService.Models.RecordModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OldRecordsService.BLL.ModelServises
{
    public class RecordModelService
    {
        #region System
        private IRepository db;

        public RecordModelService()
        {
            this.db = new Repository();
        }
        #endregion

        #region Positions
        public List<ExperienceViewModel> GetPositionsList() {

            List<ExperienceViewModel> res = new List<ExperienceViewModel>();
            try
            {
                IQueryable<cv_has_prof_exp> professions = db.GetProfessions();

                if (professions != null)
                {
                    res = professions.Select(p => new ExperienceViewModel
                    {
                        Id = p.id,
                        Position = p.position
                    }).ToList();

                    //Delete duplicate
                    List<ExperienceViewModel> resultWithoutDublicates = new List<ExperienceViewModel>();
                    int i = 0;
                    resultWithoutDublicates.Add(res[0]);
                    while (i < res.Count)
                    {
                        int j = 0;
                        bool isDublicate = false;
                        while (j < resultWithoutDublicates.Count)
                        {
                            if (resultWithoutDublicates[j].Position.Contains(res[i].Position))
                            {
                                isDublicate = true;
                                break;
                            }
                            j++;
                        }
                        if (!isDublicate)
                        {
                            resultWithoutDublicates.Add(res[i]);
                        }
                        i++;
                    }

                    res = resultWithoutDublicates;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }
        #endregion

        #region Vacancy
        public List<RecordViewModel> GetVacancyList()
        {
            List<RecordViewModel> res = new List<RecordViewModel>();
            try
            {
                IQueryable<cv> vacancies = db.GetResumes();

                if (vacancies != null)
                {
                    res = vacancies.Select(p => new RecordViewModel
                    {
                        Id = p.id,
                        Vacancy = p.position
                    }).ToList();

                    //Delete duplicate
                    List<RecordViewModel> resultWithoutDublicates = new List<RecordViewModel>();
                    int i = 0;
                    resultWithoutDublicates.Add(res[0]);
                    while (i < res.Count)
                    {
                        int j = 0;
                        bool isDublicate = false;
                        while (j < resultWithoutDublicates.Count)
                        {
                            if (resultWithoutDublicates[j].Vacancy.Contains(res[i].Vacancy))
                            {
                                isDublicate = true;
                                break;
                            }
                            j++;
                        }
                        if (!isDublicate)
                        {
                            resultWithoutDublicates.Add(res[i]);
                        }
                        i++;
                    }

                    res = resultWithoutDublicates;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }
        #endregion

        #region Status
        public List<StatusViewModel> GetStatusList()
        {
            List<StatusViewModel> res = new List<StatusViewModel>();
            try
            {
                IQueryable<cv_status> statuses = db.GetStatuses();

                if (statuses != null)
                {
                    res = statuses.Select(s => new StatusViewModel
                    {
                        Id = s.id,
                        Status = s.status,
                        Color = s.color
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }
        #endregion

        #region Resume

        private IQueryable<cv> GetSortResumeList(string sortOrder, string sortDirection, IQueryable<cv> records)
        {
            switch (sortOrder)
            {
                case "1":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.position) : records.OrderBy(r => r.position);
                    break;
                case "2":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.title) : records.OrderBy(r => r.title);
                    break;
                case "3":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.edu.name) : records.OrderBy(r => r.edu.name);
                    break;
                case "4":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.age) : records.OrderBy(r => r.age);
                    break;
                case "5":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.zp) : records.OrderBy(r => r.zp);
                    break;
                case "6":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.email) : records.OrderBy(r => r.email);
                    break;
                case "7":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.phone) : records.OrderBy(r => r.phone);
                    break;
                case "8":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.comment) : records.OrderBy(r => r.comment);
                    break;
                case "9":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.send_date) : records.OrderBy(r => r.send_date);
                    break;
                case "10":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.komatsu_comment) : records.OrderBy(r => r.komatsu_comment);
                    break;
                case "11":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.army) : records.OrderBy(r => r.army);
                    break;
                case "12":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.voditel) : records.OrderBy(r => r.voditel);
                    break;
                case "13":
                    records = sortDirection == "desc" ? records.OrderByDescending(r => r.cv_status.status) : records.OrderBy(r => r.cv_status.status);
                    break;
            }

            return records;
        }

        public List<RecordViewModel> GetResumeList(string sortOrder, string sortDirection, List<string> positions, List<string> statuses, List<string> vacancies, bool? showOldRecord, DateTime? dateFrom, DateTime? dateTo, string search, bool showEducation, bool showExperience)
        {
            List<RecordViewModel> res = new List<RecordViewModel>();
            try
            {
                //Get all records
                IQueryable<cv> records = db.GetResumes();

                records = GetSortResumeList(sortOrder, sortDirection, records);

                if (records != null)
                {
                    res = records.Select(r => new RecordViewModel
                    {
                        Id = r.id,
                        IdOld = r.id_old,
                        Age = r.age,
                        FirstName = r.first_name,
                        Title = r.title,
                        SurName = r.sur_name,
                        Birthday = r.birthday,
                        Vacancy = r.position,
                        Salary = r.zp,
                        Email = r.email,
                        Phone = r.phone,
                        Comment = r.comment,
                        IsSend = r.is_send,
                        SendDate = r.send_date,
                        Hobby = r.hobby,
                        Sport = r.sport,
                        SportProgress = r.sport_progress,
                        KomatsuComment = r.komatsu_comment,
                        Army = r.army,
                        Driver = r.voditel,
                        OccupationalSafetyHealth = r.ohranatruda,
                        IsViewed = r.prosm,
                        IsAgreePersonalData = r.is_agree_personal_data,
                        Status = new StatusViewModel
                        {
                            Id = r.cv_status.id,
                            Status = r.cv_status.status,
                            Color = r.cv_status.color
                        },
                        ExperienceAvailable = new ExperienceAvailableViewModel
                        {
                            Id = r.exp.id,
                            Period = r.exp.period
                        },
                        EducationAvailable = new EducationAvailableViewModel
                        {
                            Id = r.edu.id,
                            Name = r.edu.name
                        }
                    }).ToList();

                    //Add information about education
                    if (showEducation)
                    {
                        foreach (var item in res)
                        {
                            IQueryable<cv_has_edu_exp> recordEducations = db.GetEducations().Where(r => r.cv_id == item.IdOld);
                            var educations = recordEducations.Select(r => new EducationViewModel
                            {
                                Id = r.id,
                                CvId = r.cv_id,
                                Name = r.name,
                                FacultyName = r.fac_name,
                                Diploma = r.diplom,
                                EndYear = r.end_year
                            }).ToList();

                            item.Education = educations;
                        }
                    }
                    
                    //Add information about work experience
                    if (showExperience)
                    {
                        foreach (var item in res)
                        {
                            IQueryable<cv_has_prof_exp> recordExperiences = db.GetProfessions().Where(r => r.cv_id == item.IdOld);
                            var experiences = recordExperiences.Select(r => new ExperienceViewModel
                            {
                                Id = r.id,
                                CvId = r.cv_id,
                                BeginDate = r.begin_date,
                                EndDate = r.end_date,
                                Duty = r.duty,
                                Organisation = r.org,
                                Position = r.position
                            }).ToList();

                            item.Experience = experiences;
                        }
                    }
                    
                    //Delete entries with a date value of null
                    if ((showOldRecord != null) && (showOldRecord == false))
                    {
                        int i = 0;
                        while (i < res.Count)
                        {
                            if (res[i].SendDate == null)
                            {
                                res.RemoveAt(i);
                            }
                            else
                            {
                                i++;
                            }
                        }
                    }

                    //Delete entries with a date value less than the set value
                    if (dateFrom != null)
                    {
                        int i = 0;
                        while (i < res.Count)
                        {
                            if (res[i].SendDate < dateFrom)
                            {
                                res.RemoveAt(i);
                            }
                            else
                            {
                                i++;
                            }
                        }
                    }

                    //Delete entries with a date value greater than the set value
                    if (dateTo != null)
                    {
                        int i = 0;
                        while (i < res.Count)
                        {
                            if (res[i].SendDate > dateTo)
                            {
                                res.RemoveAt(i);
                            }
                            else
                            {
                                i++;
                            }
                        }
                    }

                    //Delete entries that do not have any of the set statuses
                    if (statuses.Count > 0)
                    {
                        int i = 0;
                        while (i < res.Count)
                        {
                            if (!statuses.Contains(res[i].Status.Status))
                            {
                                res.RemoveAt(i);
                            }
                            else
                            {
                                i++;
                            }
                        }
                    }

                    //Delete entries that do not have any of the set vacancies
                    if (vacancies.Count > 0)
                    {
                        int i = 0;
                        while (i < res.Count)
                        {
                            if (!vacancies.Contains(res[i].Vacancy))
                            {
                                res.RemoveAt(i);
                            }
                            else
                            {
                                i++;
                            }
                        }
                    }

                    //Delete entries that do not have any of the above positions in the work experience
                    if (positions.Count > 0)
                    {
                        //Browse all records
                        int i = 0;
                        while (i < res.Count)
                        {
                            //If at least one job was a post, then leave a record, otherwise delete it
                            var recordExperience = res[i].Experience;
                            bool isPositionExist = false;
                            foreach (var exp in recordExperience)
                            {
                                if (positions.Contains(exp.Position)) 
                                {
                                    isPositionExist = true;
                                    break;
                                }
                            }
                            if (!isPositionExist)
                            {
                                res.RemoveAt(i);
                            }
                            else
                            {
                                i++;
                            }
                        }
                    }

                    //Search for the occurrence of a search string in at least one field
                    if ((search != null) && (search != ""))
                    {
                        int i = 0;
                        while (i < res.Count)
                        {
                            if (res[i].Status.ToString().Contains(search) ||
                                res[i].FirstName.ToString().Contains(search) ||
                                res[i].Title.ToString().Contains(search) ||
                                res[i].SurName.ToString().Contains(search) ||
                                res[i].Age.ToString().Contains(search) ||
                                res[i].Birthday.ToString().Contains(search) ||
                                res[i].Vacancy.ToString().Contains(search) ||
                                res[i].Salary.ToString().Contains(search) ||
                                res[i].Email.ToString().Contains(search) ||
                                res[i].Phone.ToString().Contains(search) ||
                                res[i].Comment.ToString().Contains(search) ||
                                res[i].SendDate.ToString().Contains(search) ||
                                res[i].Hobby.ToString().Contains(search) ||
                                res[i].Sport.ToString().Contains(search) ||
                                res[i].SportProgress.ToString().Contains(search) ||
                                res[i].KomatsuComment.ToString().Contains(search) ||
                                res[i].Army.ToString().Contains(search) ||
                                res[i].Driver.ToString().Contains(search) ||
                                res[i].OccupationalSafetyHealth.ToString().Contains(search) ||
                                res[i].IsViewed.ToString().Contains(search))
                            {
                                i++;
                            }
                            else
                            {
                                res.RemoveAt(i);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }

        public bool GetIsResumeExist(int idOld)
        {
            bool res = false;

            List<cv> records = db.GetResumes().Where(r => r.id_old == idOld).ToList();

            if (records.Count > 0)
            {
                res = true;
            }

            return res;
        }

        public bool SetResume(CvModel record)
        {
            bool res = false;

            try
            {
                cv newRecord = new cv
                {
                    age = record.Age,
                    army = StringHelper.RemoveUnnecessaryChars(record.Army),
                    id_old = record.IdOld,
                    birthday = DateHelper.GetDateFromString(record.Birthday),
                    comment = StringHelper.RemoveUnnecessaryChars(record.Comment),
                    cv_session = StringHelper.RemoveUnnecessaryChars(record.CvSession),
                    cv_status_id = record.StatusId,
                    edu_id = record.EducationId,
                    email = StringHelper.RemoveUnnecessaryChars(record.Email),
                    exp_id = record.ExperienceId,
                    first_name = StringHelper.RemoveUnnecessaryChars(record.FirstName),
                    hobby = StringHelper.RemoveUnnecessaryChars(record.Hobby),
                    is_agree_personal_data = record.IsAgreePersonalData,
                    is_send = record.IsSend,
                    komatsu_comment = StringHelper.RemoveUnnecessaryChars(record.KomatsuComment),
                    ohranatruda = StringHelper.RemoveUnnecessaryChars(record.OccupationalSafetyHealth),
                    phone = StringHelper.RemoveUnnecessaryChars(record.Phone),
                    position = StringHelper.RemoveUnnecessaryChars(record.Vacancy),
                    prosm = record.IsViewed,
                    publish = record.IsPublish,
                    send_date = DateHelper.GetDateFromString(record.SendDate),
                    sport = StringHelper.RemoveUnnecessaryChars(record.Sport),
                    sport_progress = StringHelper.RemoveUnnecessaryChars(record.SportProgress),
                    sur_name = StringHelper.RemoveUnnecessaryChars(record.SurName),
                    title = StringHelper.RemoveUnnecessaryChars(record.Title),
                    voditel = StringHelper.RemoveUnnecessaryChars(record.Driver),
                    zp = StringHelper.RemoveUnnecessaryChars(record.Salary)
                };

                cv createdRecord = db.SaveResume(newRecord);
                if (createdRecord != null)
                {
                    res = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }

        public RecordViewModel GetResumeById(int id)
        {
            RecordViewModel res = new RecordViewModel();

            try
            {
                //Get record by id
                cv record = db.GetResumeById(id);

                if (record != null)
                {
                    res.Id = record.id;
                    res.IdOld = record.id_old;
                    res.Age = record.age;
                    res.Army = record.army;
                    res.Birthday = record.birthday;
                    res.Comment = record.comment;
                    res.Driver = record.voditel;
                    res.Email = record.email;
                    res.FirstName = record.first_name;
                    res.Hobby = record.hobby;
                    res.IsAgreePersonalData = record.is_agree_personal_data;
                    res.IsSend = record.is_send;
                    res.IsViewed = record.prosm;
                    res.KomatsuComment = record.komatsu_comment;
                    res.OccupationalSafetyHealth = record.ohranatruda;
                    res.Phone = record.phone;
                    res.Salary = record.zp;
                    res.SendDate = record.send_date;
                    res.Sport = record.sport;
                    res.SportProgress = record.sport_progress;
                    res.SurName = record.sur_name;
                    res.Title = record.title;
                    res.Vacancy = record.position;
                    res.ExperienceAvailable = new ExperienceAvailableViewModel
                    {
                        Id = record.exp.id,
                        Period = record.exp.period
                    };
                    res.EducationAvailable = new EducationAvailableViewModel
                    {
                        Id = record.edu.id,
                        Name = record.edu.name
                    };
                    res.Status = new StatusViewModel
                    {
                        Id = record.cv_status.id,
                        Status = record.cv_status.status,
                        Color = record.cv_status.color
                    };

                    //Add information about education
                    IQueryable<cv_has_edu_exp> recordEducations = db.GetEducations().Where(r => r.cv_id == res.IdOld);
                    var educations = recordEducations.Select(r => new EducationViewModel
                    {
                        Id = r.id,
                        CvId = r.cv_id,
                        Name = r.name,
                        FacultyName = r.fac_name,
                        Diploma = r.diplom,
                        EndYear = r.end_year
                    }).ToList();

                    res.Education = educations;

                    //Add information about work experience
                    IQueryable<cv_has_prof_exp> recordExperiences = db.GetProfessions().Where(r => r.cv_id == res.IdOld);
                    var experiences = recordExperiences.Select(r => new ExperienceViewModel
                    {
                        Id = r.id,
                        CvId = r.cv_id,
                        BeginDate = r.begin_date,
                        EndDate = r.end_date,
                        Duty = r.duty,
                        Organisation = r.org,
                        Position = r.position
                    }).ToList();

                    res.Experience = experiences;

                    //Add information about languages
                    IQueryable<cv_has_lang_exp> recordLanguages = db.GetLanguages().Where(r => r.cv_id == res.IdOld);
                    var languages = recordLanguages.Select(r => new LanguageViewModel
                    {
                        Id = r.id,
                        CvId = r.cv_id,
                        Extent = r.extent,
                        IdOld = r.id_old,
                        Name = r.name
                    }).ToList();

                    res.Languages = languages;

                    //Add information about tests
                    IQueryable<cv_has_test_exp> recordTests = db.GetTests().Where(r => r.cv_id == res.IdOld);
                    var tests = recordTests.Select(r => new TestViewModel
                    {
                        Id = r.id,
                        CvId = r.cv_id,
                        Organisation = r.org,
                        EndYear = r.end_year,
                        IdOld = r.id_old,
                        Result = r.result,
                        Name = r.name
                    }).ToList();

                    res.Tests = tests;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }
        #endregion

        #region Education
        public List<EducationViewModel> GetEducationList()
        {
            List<EducationViewModel> res = new List<EducationViewModel>();
            try
            {
                //Get all records
                IQueryable<cv_has_edu_exp> records = db.GetEducations();

                if (records != null)
                {
                    res = records.Select(r => new EducationViewModel
                    {
                        Id = r.id,
                        IdOld = r.id_old,
                        CvId = r.cv_id,
                        Name = r.name,
                        FacultyName = r.fac_name,
                        Diploma = r.diplom,
                        EndYear = r.end_year
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }

        public bool GetIsEducationExist(int idOld)
        {
            bool res = false;

            List<cv_has_edu_exp> records = db.GetEducations().Where(r => r.id_old == idOld).ToList();

            if (records.Count > 0)
            {
                res = true;
            }

            return res;
        }

        public bool SetEducation(CvEduModel record)
        {
            bool res = false;

            try
            {
                cv_has_edu_exp newRecord = new cv_has_edu_exp
                {
                    cv_id = record.CvId,
                    id_old = record.IdOld,
                    diplom = StringHelper.RemoveUnnecessaryChars(record.Diploma),
                    end_year = record.EndYear,
                    fac_name = StringHelper.RemoveUnnecessaryChars(record.FacName),
                    name = StringHelper.RemoveUnnecessaryChars(record.Name)
                };

                cv_has_edu_exp createdRecord = db.SaveEducation(newRecord);
                if (createdRecord != null)
                {
                    res = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }
        #endregion

        #region Experience
        public List<ExperienceViewModel> GetExperienceList()
        {
            List<ExperienceViewModel> res = new List<ExperienceViewModel>();
            try
            {
                //Get all records
                IQueryable<cv_has_prof_exp> records = db.GetProfessions();

                if (records != null)
                {
                    res = records.Select(r => new ExperienceViewModel
                    {
                        Id = r.id,
                        IdOld = r.id_old,
                        CvId = r.cv_id,
                        BeginDate = r.begin_date,
                        Duty = r.duty,
                        EndDate = r.end_date,
                        Organisation = r.org,
                        Position = r.position
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }

        public bool GetIsExperienceExist(int idOld)
        {
            bool res = false;

            List<cv_has_prof_exp> records = db.GetProfessions().Where(r => r.id_old == idOld).ToList();

            if (records.Count > 0)
            {
                res = true;
            }

            return res;
        }

        public bool SetExperience(CvProfModel record)
        {
            bool res = false;

            try
            {
                cv_has_prof_exp newRecord = new cv_has_prof_exp
                {
                    cv_id = record.CvId,
                    id_old = record.IdOld,
                    begin_date = DateHelper.GetDateFromString(record.BeginDate),
                    duty = StringHelper.RemoveUnnecessaryChars(record.Duty),
                    end_date = DateHelper.GetDateFromString(record.EndDate),
                    org = StringHelper.RemoveUnnecessaryChars(record.Organisation),
                    position = StringHelper.RemoveUnnecessaryChars(record.Position)
                };

                cv_has_prof_exp createdRecord = db.SaveProfession(newRecord);
                if (createdRecord != null)
                {
                    res = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }
        #endregion

        #region Language
        public List<LanguageViewModel> GetLanguageList()
        {
            List<LanguageViewModel> res = new List<LanguageViewModel>();
            try
            {
                //Get all records
                IQueryable<cv_has_lang_exp> records = db.GetLanguages();

                if (records != null)
                {
                    res = records.Select(r => new LanguageViewModel
                    {
                        Id = r.id,
                        IdOld = r.id_old,
                        CvId = r.cv_id,
                        Extent = r.extent,
                        Name = r.name,
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }

        public bool GetIsLanguageExist(int idOld)
        {
            bool res = false;

            List<cv_has_lang_exp> records = db.GetLanguages().Where(r => r.id_old == idOld).ToList();

            if (records.Count > 0)
            {
                res = true;
            }

            return res;
        }

        public bool SetLanguage(CvLangModel record)
        {
            bool res = false;

            try
            {
                cv_has_lang_exp newRecord = new cv_has_lang_exp
                {
                    cv_id = record.CvId,
                    id_old = record.IdOld,
                    extent = StringHelper.RemoveUnnecessaryChars(record.Extent),
                    name = StringHelper.RemoveUnnecessaryChars(record.Name)
                };

                cv_has_lang_exp createdRecord = db.SaveLanguage(newRecord);
                if (createdRecord != null)
                {
                    res = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }
        #endregion

        #region Test
        public List<TestViewModel> GetTestList()
        {
            List<TestViewModel> res = new List<TestViewModel>();
            try
            {
                //Get all records
                IQueryable<cv_has_test_exp> records = db.GetTests();

                if (records != null)
                {
                    res = records.Select(r => new TestViewModel
                    {
                        Id = r.id,
                        IdOld = r.id_old,
                        CvId = r.cv_id,
                        EndYear = r.end_year,
                        Organisation = r.org,
                        Result = r.result,
                        Name = r.name
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }

        public bool GetIsTestExist(int idOld)
        {
            bool res = false;

            List<cv_has_test_exp> records = db.GetTests().Where(r => r.id_old == idOld).ToList();

            if (records.Count > 0)
            {
                res = true;
            }

            return res;
        }

        public bool SetTest(CvTestModel record)
        {
            bool res = false;

            try
            {
                cv_has_test_exp newRecord = new cv_has_test_exp
                {
                    cv_id = record.CvId,
                    id_old = record.IdOld,
                    name = StringHelper.RemoveUnnecessaryChars(record.Name),
                    end_year = record.EndYear,
                    org = StringHelper.RemoveUnnecessaryChars(record.Organisation),
                    result = StringHelper.RemoveUnnecessaryChars(record.Result)
                };

                cv_has_test_exp createdRecord = db.SaveTest(newRecord);
                if (createdRecord != null)
                {
                    res = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Message);
            }
            return res;
        }
        #endregion

    }
}