﻿using OldRecordsService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldRecordsService.BLL.Interfaces
{
    interface IRepository
    {
        #region System
        void Save();
        #endregion

        #region cv
        IQueryable<cv> GetResumes();

        cv SaveResume(cv item);

        cv EditResume(cv item);

        cv GetResumeById(int id);
        #endregion

        #region cv_has_edu_exp
        IQueryable<cv_has_edu_exp> GetEducations();

        cv_has_edu_exp SaveEducation(cv_has_edu_exp item);

        cv_has_edu_exp EditEducation(cv_has_edu_exp item);

        cv_has_edu_exp GetEducationById(int id);
        #endregion

        #region cv_has_lang_exp
        IQueryable<cv_has_lang_exp> GetLanguages();

        cv_has_lang_exp SaveLanguage(cv_has_lang_exp item);

        cv_has_lang_exp EditLanguage(cv_has_lang_exp item);

        cv_has_lang_exp GetLanguageById(int id);
        #endregion

        #region cv_has_prof_exp
        IQueryable<cv_has_prof_exp> GetProfessions();

        cv_has_prof_exp SaveProfession(cv_has_prof_exp item);

        cv_has_prof_exp EditProfession(cv_has_prof_exp item);

        cv_has_prof_exp GetProfessionById(int id);
        #endregion

        #region cv_has_test_exp
        IQueryable<cv_has_test_exp> GetTests();

        cv_has_test_exp SaveTest(cv_has_test_exp item);

        cv_has_test_exp EditTest(cv_has_test_exp item);

        cv_has_test_exp GetTestById(int id);
        #endregion

        #region cv_status
        IQueryable<cv_status> GetStatuses();

        cv_status SaveStatus(cv_status item);

        cv_status EditStatus(cv_status item);

        cv_status GetStatusById(int id);
        #endregion

        #region edu
        IQueryable<edu> GetEducationsAvailable();

        edu SaveEducationAvailable(edu item);

        edu EditEducationAvailable(edu item);

        edu GetEducationAvailableById(int id);
        #endregion

        #region exp
        IQueryable<exp> GetEperiences();

        exp SaveEperience(exp item);

        exp EditEperience(exp item);

        exp GetEperienceById(int id);
        #endregion
    }
}
