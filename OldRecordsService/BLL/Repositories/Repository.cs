﻿using OldRecordsService.BLL.Interfaces;
using OldRecordsService.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OldRecordsService.BLL.Repositories
{
    public class Repository : IRepository
    {
        private RecordDBEntities db = new RecordDBEntities();

        #region System
        public void Save()
        {
            db.SaveChanges();
        }
        #endregion

        #region cv
        public IQueryable<cv> GetResumes()
        {
            var res = db.cv;
            return res;
        }

        public cv SaveResume(cv item)
        {
            db.cv.Add(item);
            Save();
            return item;
        }

        public cv EditResume(cv item)
        {
            db.cv.Add(item);
            db.Entry(item).State = EntityState.Modified;
            Save();
            return item;
        }

        public cv GetResumeById(int id)
        {
            var res = db.cv.FirstOrDefault(d => d.id == id);
            return res;
        }
        #endregion

        #region cv_has_edu_exp
        public IQueryable<cv_has_edu_exp> GetEducations()
        {
            var res = db.cv_has_edu_exp;
            return res;
        }

        public cv_has_edu_exp SaveEducation(cv_has_edu_exp item)
        {
            db.cv_has_edu_exp.Add(item);
            Save();
            return item;
        }

        public cv_has_edu_exp EditEducation(cv_has_edu_exp item)
        {
            db.cv_has_edu_exp.Add(item);
            db.Entry(item).State = EntityState.Modified;
            Save();
            return item;
        }

        public cv_has_edu_exp GetEducationById(int id)
        {
            var res = db.cv_has_edu_exp.FirstOrDefault(d => d.id == id);
            return res;
        }
        #endregion

        #region cv_has_lang_exp
        public IQueryable<cv_has_lang_exp> GetLanguages()
        {
            var res = db.cv_has_lang_exp;
            return res;
        }

        public cv_has_lang_exp SaveLanguage(cv_has_lang_exp item)
        {
            db.cv_has_lang_exp.Add(item);
            Save();
            return item;
        }

        public cv_has_lang_exp EditLanguage(cv_has_lang_exp item)
        {
            db.cv_has_lang_exp.Add(item);
            db.Entry(item).State = EntityState.Modified;
            Save();
            return item;
        }

        public cv_has_lang_exp GetLanguageById(int id)
        {
            var res = db.cv_has_lang_exp.FirstOrDefault(d => d.id == id);
            return res;
        }
        #endregion

        #region cv_has_prof_exp
        public IQueryable<cv_has_prof_exp> GetProfessions()
        {
            var res = db.cv_has_prof_exp;
            return res;
        }

        public cv_has_prof_exp SaveProfession(cv_has_prof_exp item)
        {
            db.cv_has_prof_exp.Add(item);
            Save();
            return item;
        }

        public cv_has_prof_exp EditProfession(cv_has_prof_exp item)
        {
            db.cv_has_prof_exp.Add(item);
            db.Entry(item).State = EntityState.Modified;
            Save();
            return item;
        }

        public cv_has_prof_exp GetProfessionById(int id)
        {
            var res = db.cv_has_prof_exp.FirstOrDefault(d => d.id == id);
            return res;
        }
        #endregion

        #region cv_has_test_exp
        public IQueryable<cv_has_test_exp> GetTests()
        {
            var res = db.cv_has_test_exp;
            return res;
        }

        public cv_has_test_exp SaveTest(cv_has_test_exp item)
        {
            db.cv_has_test_exp.Add(item);
            Save();
            return item;
        }

        public cv_has_test_exp EditTest(cv_has_test_exp item)
        {
            db.cv_has_test_exp.Add(item);
            db.Entry(item).State = EntityState.Modified;
            Save();
            return item;
        }

        public cv_has_test_exp GetTestById(int id)
        {
            var res = db.cv_has_test_exp.FirstOrDefault(d => d.id == id);
            return res;
        }
        #endregion

        #region cv_status
        public IQueryable<cv_status> GetStatuses()
        {
            var res = db.cv_status;
            return res;
        }

        public cv_status SaveStatus(cv_status item)
        {
            db.cv_status.Add(item);
            Save();
            return item;
        }

        public cv_status EditStatus(cv_status item)
        {
            db.cv_status.Add(item);
            db.Entry(item).State = EntityState.Modified;
            Save();
            return item;
        }

        public cv_status GetStatusById(int id)
        {
            var res = db.cv_status.FirstOrDefault(d => d.id == id);
            return res;
        }
        #endregion

        #region edu
        public IQueryable<edu> GetEducationsAvailable()
        {
            var res = db.edu;
            return res;
        }

        public edu SaveEducationAvailable(edu item)
        {
            db.edu.Add(item);
            Save();
            return item;
        }

        public edu EditEducationAvailable(edu item)
        {
            db.edu.Add(item);
            db.Entry(item).State = EntityState.Modified;
            Save();
            return item;
        }

        public edu GetEducationAvailableById(int id)
        {
            var res = db.edu.FirstOrDefault(d => d.id == id);
            return res;
        }
        #endregion

        #region exp
        public IQueryable<exp> GetEperiences()
        {
            var res = db.exp;
            return res;
        }

        public exp SaveEperience(exp item)
        {
            db.exp.Add(item);
            Save();
            return item;
        }

        public exp EditEperience(exp item)
        {
            db.exp.Add(item);
            db.Entry(item).State = EntityState.Modified;
            Save();
            return item;
        }

        public exp GetEperienceById(int id)
        {
            var res = db.exp.FirstOrDefault(d => d.id == id);
            return res;
        }
        #endregion
    }
}